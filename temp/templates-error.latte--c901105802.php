<?php

use Latte\Runtime as LR;

/** source: templates/error.latte */
final class Template_c901105802 extends Latte\Runtime\Template
{
	public const Source = 'templates/error.latte';


	public function main(array $ʟ_args): void
	{
		echo '<header class="border container-fluid">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section id="error" class="mt-5 container">
    <h3 class="text-danger">Při načítání stránky nastala chyba!</h3>
    <img  class="rounded" src="./upload/core/error.webp" alt="...">
</section>';
	}
}
