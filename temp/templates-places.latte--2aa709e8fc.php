<?php

use Latte\Runtime as LR;

/** source: templates/places.latte */
final class Template_2aa709e8fc extends Latte\Runtime\Template
{
	public const Source = 'templates/places.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<header class="border container-fluid">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
            <form action="" method="get" class="d-flex" role="search">
                <input class="form-control me-3" name="search" type="search" placeholder="Vyhledat podle země, autora, názvu článku, nebo úryvku z obsahu" aria-label="Vyhledat"';
		$ʟ_tmp = ['value' => $filtr];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 17 */;
		echo '>
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <h3 class="col-12">Destinace</h3>
        <form action="" method="get" class="d-flex">
';
		foreach ($places as $place) /* line 26 */ {
			if ($place['name'] === 'Vše') /* line 27 */ {
				echo '                    <button';
				$ʟ_tmp = ['value' => ''];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 28 */;
				echo ' class="btn btn-outline-dark col-2 m-2" name="search" type="submit">';
				echo LR\Filters::escapeHtmlText($place['name']) /* line 28 */;
				echo '</button>
';
			}
			if ($place['name'] !== 'Vše') /* line 30 */ {
				echo '                    <button';
				$ʟ_tmp = ['value' => $place['name']];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 31 */;
				echo ' class="btn btn-outline-dark col-2 m-2" name="search" type="submit">';
				echo LR\Filters::escapeHtmlText($place['name']) /* line 31 */;
				echo '</button>
';
			}

		}

		echo '        </form>
    </div>
    <div class="container mt-5">
        <h3 class="col-12">Autor</h3>
        <form action="" method="get" class="d-flex">
';
		foreach ($users as $user) /* line 39 */ {
			echo '                    <button';
			$ʟ_tmp = ['value' => $user['login']];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 40 */;
			echo ' class="btn btn-outline-dark col-2 m-2" name="search" type="submit">';
			echo LR\Filters::escapeHtmlText($user['login']) /* line 40 */;
			echo '</button>
';

		}

		echo '        </form>
    </div>
    <hr class="mt-5">
    <div class="container-fluid places p-5">
';
		foreach ($blogs as $blog) /* line 46 */ {
			echo "\n";
			if (strpos(strtolower($blog['title']), strtolower($filtr)) !== false || strpos(strtolower($users[$blog['autor_id']]['login']), strtolower($filtr)) !== false || strpos(strtolower($places[$blog['place_id']]['name']), strtolower($filtr)) !== false || strpos(strtolower($blog['article']), strtolower($filtr)) !== false || $filtr == '') /* line 48 */ {
				echo "\n";
				$url = $places[$blog['place_id']]['img_url'] /* line 56 */;
				$id = $blog['id'] /* line 57 */;
				echo '                    <a class="place card"';
				$ʟ_tmp = ['href' => ' ?page=blog&blog=' . $id];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 58 */;
				echo '>
                        <div class="img"';
				$ʟ_tmp = ['style' => 'background-image: url(' . $url . ');', 'href' => '?page=blog&blog=' . $id];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 59 */;
				echo '>

                        </div>
                        <div class="bg-white p-3">
                            <h2>';
				echo LR\Filters::escapeHtmlText($blog['title']) /* line 63 */;
				echo '</h2> <h3> od ';
				echo LR\Filters::escapeHtmlText($users[$blog['autor_id']]['login']) /* line 63 */;
				echo ' </h3>
                            <p> ';
				echo LR\Filters::escapeHtmlText(($this->filters->limitWords)($blog['article'], 20)) /* line 64 */;
				echo '</p>
                        </div>
                    </a>
';
			}

		}

		echo '    </div>
</section>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['place' => '26', 'user' => '39', 'blog' => '46'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
