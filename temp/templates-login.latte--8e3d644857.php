<?php

use Latte\Runtime as LR;

/** source: templates/login.latte */
final class Template_8e3d644857 extends Latte\Runtime\Template
{
	public const Source = 'templates/login.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<header class="border container-fluid">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section class="container">
    <form action="" method="post" class="mt-5">
        <label class="form-label" for="login">Login:</label>
        <input class="form-control" type="text" name="login">
        <br>
        <label class="form-label" for="password">Heslo</label>
        <input class="form-control" type="password" name="password">
        <br>
        <input class="btn btn-dark" type="submit" value="Přihlásit" name="log">
        <br>
        <br>
';
		if ($loggingIn) /* line 23 */ {
			if ($logged) /* line 24 */ {
				echo '                <h2 class="text-success">Povedlo se přihlásit</h2>
';
			}
			if (!$logged) /* line 27 */ {
				echo '                <h2 class="text-danger">Nepovedlo se přihlásit</h2>
';
			}
		}
		echo '    </form>
</section>';
	}
}
