<?php

use Latte\Runtime as LR;

/** source: templates/blog.latte */
final class Template_75fd4265bb extends Latte\Runtime\Template
{
	public const Source = 'templates/blog.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<header class="border container-fluid">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid" class="position-absolute top-0 start-0">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section id="blog">
    <h1>';
		echo LR\Filters::escapeHtmlText($blogs[$blogID]['title']) /* line 13 */;
		echo '</h1>
';
		$src = $users[$blogs[$blogID]['autor_id']]['img_url'] /* line 14 */;
		echo '    <img';
		$ʟ_tmp = ['src' => $src];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 15 */;
		echo ' alt="..." class="image1">
    <h2>';
		echo LR\Filters::escapeHtmlText($users[$blogs[$blogID]['autor_id']]['login']) /* line 16 */;
		echo '</h2>
';
		$src2 = $blogs[$blogID]['img_url'] /* line 17 */;
		echo '    <p><div class="title">';
		echo LR\Filters::escapeHtmlText($places[$blogs[$blogID]['place_id']]['name']) /* line 18 */;
		echo '</div>
    <img';
		$ʟ_tmp = ['src' => $src2];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 19 */;
		echo ' alt="..." class="rounded mx-auto d-block">
    <div class="text">';
		echo LR\Filters::escapeHtmlText($blogs[$blogID]['article']) /* line 20 */;
		echo '</div></p>
</section>';
	}
}
