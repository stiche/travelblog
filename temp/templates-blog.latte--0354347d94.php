<?php

use Latte\Runtime as LR;

/** source: templates/blog.latte */
final class Template_0354347d94 extends Latte\Runtime\Template
{
	public const Source = 'templates/blog.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<header class="border container-fluid d-flex">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section class="container mt-5" id="blog">
    <h1 class="text-fluid">';
		echo LR\Filters::escapeHtmlText($blogs[$blogID]['title']) /* line 13 */;
		echo '</h1>

';
		$src = $users[$blogs[$blogID]['autor_id']]['img_url'] /* line 15 */;
		echo '    <div class="blog-autor pt-2">
        <img class="rounded-circle"';
		$ʟ_tmp = ['src' => $src];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 17 */;
		echo ' alt="...">
        <h3>';
		echo LR\Filters::escapeHtmlText($users[$blogs[$blogID]['autor_id']]['login']) /* line 18 */;
		echo '</h3>
    </div>
';
		$src2 = $blogs[$blogID]['img_url'] /* line 20 */;
		echo '    <div class="pt-2">
        <h3 class="blog-autor pt-2">';
		echo LR\Filters::escapeHtmlText($places[$blogs[$blogID]['place_id']]['name']) /* line 22 */;
		echo '</h3>
    </div>
    <br>
    <div class="container-fluid blog-img">
        <img';
		$ʟ_tmp = ['src' => $src2];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 26 */;
		echo ' alt="...">
    </div>
    <p>
    <div>';
		echo LR\Filters::escapeHtmlText($blogs[$blogID]['article']) /* line 29 */;
		echo '</div></p>
</section>';
	}
}
