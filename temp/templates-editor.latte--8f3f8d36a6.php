<?php

use Latte\Runtime as LR;

/** source: templates/editor.latte */
final class Template_8f3f8d36a6 extends Latte\Runtime\Template
{
	public const Source = 'templates/editor.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<section class="container-fluid">
';
		$names = ['?', 'delegáta', 'administrátora'] /* line 2 */;
		echo '    <h1 class="pt-5">Profil ';
		echo LR\Filters::escapeHtmlText($names[$users[$loggedId]['power']]) /* line 3 */;
		echo ': <i>';
		echo LR\Filters::escapeHtmlText($users[$loggedId]['login']) /* line 3 */;
		echo '</i></h1>
    <br>
    <hr>
    <br>
    <form action="?page=editor" method="post" class="container border p-2 rounded">
        <input type="hidden" name="editor">
';
		if ($users[$loggedId]['power'] == 2) /* line 9 */ {
			foreach ($blogs as $blog) /* line 10 */ {
				echo '            <button type="submit" class="btn btn-dark" name="set"';
				$ʟ_tmp = ['value' => $blog['id']];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 11 */;
				echo '>';
				echo LR\Filters::escapeHtmlText($blog['title']) /* line 11 */;
				echo '</button>
';

			}

		}
		if ($users[$loggedId]['power'] == 1) /* line 14 */ {
			foreach ($blogs as $blog) /* line 15 */ {
				if ($blog['autor_id'] == $loggedId) /* line 16 */ {
					echo '                <button type="submit" name="set" class="btn btn-dark"';
					$ʟ_tmp = ['value' => $blog['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 17 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($blog['title']) /* line 17 */;
					echo '</button>
';
				}

			}

		}
		echo '        <button type="submit" class="btn btn-outline-light" name="set" value=""><i style="cursor: pointer;" class="material-icons">add</i></button>
    </form>
    <br>
    <hr>
    <br>
    <form action="?page=editor" method="post">
        <input type="hidden" name="editor">
        <input type="hidden" name="blogId"';
		$ʟ_tmp = ['value' => $formView];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 28 */;
		echo '>
        <label for="title" class="form-label">Název článků: </label>
';
		if ($formView != '') /* line 30 */ {
			$title = $blogs[$formView]['title'] /* line 31 */;
			echo '        <input type="text" class="form-control" name="title"';
			$ʟ_tmp = ['value' => $title];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 32 */;
			echo '>
';
		}
		if ($formView == '') /* line 34 */ {
			echo '        <input type="text" class="form-control" name="title">
';
		}
		echo '        <br>
        <br>
        <label for="place" class="form-label">Místo pro článek:</label>
';
		if ($formView != '') /* line 40 */ {
			$placeView = $blogs[$formView]['place_id'] /* line 41 */;
			echo '        <select name="place" class="form-select">
';
			foreach ($places as $place) /* line 43 */ {
				if ($placeView == $place['id']) /* line 44 */ {
					echo '                <option selected';
					$ʟ_tmp = ['value' => $place['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 45 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($place['name']) /* line 45 */;
					echo '</option>
';
				}
				if ($placeView != $place['id']) /* line 47 */ {
					echo '                <option';
					$ʟ_tmp = ['value' => $place['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 48 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($place['name']) /* line 48 */;
					echo '</option>
';
				}

			}

			echo '        </select>
';
		}
		if ($formView == '') /* line 53 */ {
			echo '        <select name="place" class="form-select">
';
			foreach ($places as $place) /* line 55 */ {
				echo '                <option';
				$ʟ_tmp = ['value' => $place['id']];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 56 */;
				echo '>';
				echo LR\Filters::escapeHtmlText($place['name']) /* line 56 */;
				echo '</option>
';

			}

			echo '        </select>
';
		}
		echo '        <br>
        <br>
        <label for="article" class="form-label">Text článku: </label>
';
		if ($formView != '') /* line 63 */ {
			$article = $blogs[$formView]['article'] /* line 64 */;
			echo '            <textarea class="form-control" name="article" cols="100" rows="10">
                ';
			echo LR\Filters::escapeHtmlText($article) /* line 66 */;
			echo '
            </textarea>
';
		}
		if ($formView == '') /* line 69 */ {
			echo '            <textarea class="form-control" name="article" cols="100" rows="10"></textarea>
';
		}
		echo '        <br>
        <br>
';
		if ($formView != '') /* line 74 */ {
			$url = $blogs[$formView]['img_url'] /* line 75 */;
			echo '        <img';
			$ʟ_tmp = ['src' => $url];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 76 */;
			echo ' alt="..." style="height: 40vh">
';
		}
		if ($formView == '') /* line 78 */ {
			echo '        <img src="./upload/core/error.webp" alt="..." style="height: 40vh">
';
		}
		echo '        <br>
        <br>
        <label for="image" class="form-label">Obrázek: </label>
        <br>
        <select name="img" class="form-select" onchange="renew()">
';
		foreach ($imgPaths as $paths) /* line 86 */ {
			echo '                <option';
			$ʟ_tmp = ['value' => './upload/blogs/' . $paths];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 87 */;
			echo ' class="path">';
			echo LR\Filters::escapeHtmlText($paths) /* line 87 */;
			echo '</option>
';

		}

		echo '            <option value="./upload/core/error.webp" class="path">error.webp</option>
        </select>
        <br>
        <input type="file" name="image">
        <br>
        <br>
        <input type="submit" value="Nahrát obrázek" name="imgUp" class="btn btn-dark">
        <br>
        <br>
';
		if ($formView == '') /* line 98 */ {
			echo '            <input type="submit" value="Uložit nový článek" name="new" class="btn btn-success">
';
		}
		if ($formView != '') /* line 101 */ {
			echo '            <input type="submit" value="Uložit změny" name="save" class="btn btn-success">
';
		}
		echo '        <input type="submit" value="Zahodit změny" name="clear" class="btn btn-danger">
';
		if ($formError) /* line 105 */ {
			echo '            <br>
            <div class="card m-2 mt-5 p-2 text-center bg-danger text-light">Chyba ve formuláři</div>
';
		}
		echo '    </form>
    <br>
    <hr>
    <br>
    <form action="?page=login" method="post">
        <input type="submit" value="Odhlásit se" name="logOUT" class="btn btn-dark">
    </form>
    <br>
    <br>
</section>
<script>
    function renew() {
        
        let imgs = document.querySelectorAll(".path");

        for(let i = 0;i < imgs.length;i++) {
            if(imgs[i].selected) {
                
                document.querySelector("img").src = imgs[i].value;
            }
        }
    }
</script>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['blog' => '10, 15', 'place' => '43, 55', 'paths' => '86'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
