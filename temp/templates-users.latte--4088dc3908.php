<?php

use Latte\Runtime as LR;

/** source: templates/users.latte */
final class Template_4088dc3908 extends Latte\Runtime\Template
{
	public const Source = 'templates/users.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<header class="border container-fluid">
    <h1 class="p-3">
        Cestilo
    </h1>
    <nav class="grid">
        <a class="m-2 btn btn-dark" href="?page=landing">Domů</a>
        <a class="m-2 btn btn-dark" href="?page=places">Místa</a>
        <a class="m-2 btn btn-dark" href="?page=users">Uživatelé</a>
        <a class="p-2 material-icons" href="?page=login">account_circle</a>
    </nav>
</header>
<section id="users" class="container mt-5">
';
		foreach ($users as $user) /* line 13 */ {
			$url2 = $user['img_url'] /* line 14 */;
			echo '        <div class="card">
            <h3 class="text-center p-3">';
			echo LR\Filters::escapeHtmlText($user['login']) /* line 16 */;
			echo '</h3>
            <div';
			$ʟ_tmp = ['style' => 'background-image: url(' . $url2 . ')'];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 17 */;
			echo '></div>
        </div>
';

		}

		echo '</section>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['user' => '13'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
