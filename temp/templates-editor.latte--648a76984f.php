<?php

use Latte\Runtime as LR;

/** source: templates/editor.latte */
final class Template_648a76984f extends Latte\Runtime\Template
{
	public const Source = 'templates/editor.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<section class="container-fluid">
';
		$names = ['?', 'delegáta', 'administrátora'] /* line 2 */;
		echo '    <h1 class="pt-5">Profil ';
		echo LR\Filters::escapeHtmlText($names[$users[$loggedId]['power']]) /* line 3 */;
		echo ': <i>';
		echo LR\Filters::escapeHtmlText($users[$loggedId]['login']) /* line 3 */;
		echo '</i></h1>
    <br>
    <hr>
    <br>
    <form action="?page=editor" method="post" class="container border p-2 rounded">
        <input type="hidden" name="editor">
';
		if ($users[$loggedId]['power'] == 2) /* line 9 */ {
			foreach ($blogs as $blog) /* line 10 */ {
				echo '            <button type="submit" class="btn btn-dark" name="set"';
				$ʟ_tmp = ['value' => $blog['id']];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 11 */;
				echo '>';
				echo LR\Filters::escapeHtmlText($blog['title']) /* line 11 */;
				echo '</button>
';

			}

		}
		if ($users[$loggedId]['power'] == 1) /* line 14 */ {
			foreach ($blogs as $blog) /* line 15 */ {
				if ($blog['autor_id'] == $loggedId) /* line 16 */ {
					echo '                <button type="submit" name="set" class="btn btn-dark"';
					$ʟ_tmp = ['value' => $blog['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 17 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($blog['title']) /* line 17 */;
					echo '</button>
';
				}

			}

		}
		echo '        <button type="submit" class="btn btn-outline-light" name="set" value=""><i style="cursor: pointer;" class="material-icons">add</i></button>
    </form>
    <br>
    <hr>
    <br>
    <form action="?page=editor" method="post">
        <input type="hidden" name="editor">
        <label for="title" class="form-label">Název článků: </label>
';
		if ($formView != '') /* line 29 */ {
			$title = $blogs[$formView]['title'] /* line 30 */;
			echo '        <input type="text" class="form-control" name="title"';
			$ʟ_tmp = ['value' => $title];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 31 */;
			echo '>
';
		}
		if ($formView == '') /* line 33 */ {
			echo '        <input type="text" class="form-control" name="title">
';
		}
		echo '        <br>
        <br>
        <label for="place" class="form-label">Místo pro článek:</label>
';
		if ($formView != '') /* line 39 */ {
			$placeView = $blogs[$formView]['place_id'] /* line 40 */;
			echo '        <select name="place" class="form-select">
';
			foreach ($places as $place) /* line 42 */ {
				if ($placeView == $place['id']) /* line 43 */ {
					echo '                <option selected';
					$ʟ_tmp = ['value' => $place['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 44 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($place['name']) /* line 44 */;
					echo '</option>
';
				}
				if ($placeView != $place['id']) /* line 46 */ {
					echo '                <option';
					$ʟ_tmp = ['value' => $place['id']];
					echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 47 */;
					echo '>';
					echo LR\Filters::escapeHtmlText($place['name']) /* line 47 */;
					echo '</option>
';
				}

			}

			echo '        </select>
';
		}
		if ($formView == '') /* line 52 */ {
			echo '        <select name="place" class="form-select">
';
			foreach ($places as $place) /* line 54 */ {
				echo '                <option';
				$ʟ_tmp = ['value' => $place['id']];
				echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 55 */;
				echo '>';
				echo LR\Filters::escapeHtmlText($place['name']) /* line 55 */;
				echo '</option>
';

			}

			echo '        </select>
';
		}
		echo '        <br>
        <br>
        <label for="article" class="form-label">Text článku: </label>
';
		if ($formView != '') /* line 62 */ {
			$article = $blogs[$formView]['article'] /* line 63 */;
			echo '            <textarea class="form-control" name="article" cols="100" rows="10">
                ';
			echo LR\Filters::escapeHtmlText($article) /* line 65 */;
			echo '
            </textarea>
';
		}
		if ($formView == '') /* line 68 */ {
			echo '            <textarea class="form-control" name="article" cols="100" rows="10"></textarea>
';
		}
		echo '        <br>
        <br>
';
		if ($formView != '') /* line 73 */ {
			$url = $blogs[$formView]['img_url'] /* line 74 */;
			echo '        <img';
			$ʟ_tmp = ['src' => $url];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 75 */;
			echo ' alt="..." style="height: 40vh">
';
		}
		if ($formView == '') /* line 77 */ {
			echo '        <img src="./upload/core/error.webp" alt="..." style="height: 40vh">
';
		}
		echo '        <br>
        <br>
        <label for="image" class="form-label">Obrázek: </label>
        <br>
        <input type="file" name="image">
        <br>
        <br>
        <input type="submit" value="Uložit změny" name="save" class="btn btn-success">
        <input type="submit" value="Zahodit změny" name="" class="btn btn-danger">
    </form>
    <br>
    <hr>
    <br>
    <form action="?page=login" method="post">
        <input type="submit" value="Odhlásit se" name="logOUT" class="btn btn-dark">
    </form>
    <br>
    <br>
</section>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['blog' => '10, 15', 'place' => '42, 54'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
