-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 23. dub 2024, 14:37
-- Verze serveru: 10.4.24-MariaDB
-- Verze PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `blog`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `title` varchar(40) DEFAULT NULL,
  `article` varchar(4000) DEFAULT NULL,
  `img_url` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `blogs`
--

INSERT INTO `blogs` (`id`, `place_id`, `autor_id`, `title`, `article`, `img_url`) VALUES
(1, 1, 1, 'Pražský hrad', 'Pražský hrad, symbol české státnosti a jedno z nejvýznamnějších kulturních památek České republiky, se tyčí na vrcholu hradčanského kopce nad Vltavou. Jeho počátky sahají do 9. století a od té doby prošel mnoha proměnami a rozšířeními, které odrážejí různé architektonické styly od románského přes gotický až po barokní.', 'upload/blogs/hrad.webp'),
(2, 3, 0, 'Slovenské ovce', 'V mé bakalářské práci se zabývám historickým vývojem chovu ovcí na území Slovenska. Následně se zaměřuji na vznik slovenské dojné ovce, a to z důrazem na jednotlivá plemena podílející se šlechtitelském procesu. Současně bylo cílem mé práce zhodnotit výsledky kontroly užitkovosti slovenské dojné ovce. ', 'upload/blogs/sheep.webp');

-- --------------------------------------------------------

--
-- Struktura tabulky `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `img_url` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `places`
--

INSERT INTO `places` (`id`, `name`, `img_url`) VALUES
(0, 'Vše', NULL),
(1, 'Česká Republika', 'upload/places/cesko.webp'),
(2, 'Německo', 'upload/places/nemecko.webp'),
(3, 'Slovensko', 'upload/places/slovensko.webp');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `img_url` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `power`, `img_url`) VALUES
(0, 'admin', 'admin', 2, 'upload/users/admin.webp'),
(1, 'user1', 'heslo', 1, 'upload/users/user1.webp');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
