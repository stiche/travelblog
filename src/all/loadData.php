<?php
    
    // objekt pro práci s databází
    class dataMaster{

        private $conn;

        public function setConn($servername,$username,$password,$database) {

            $this->conn = new mysqli($servername, $username, $password, $database);
        }

        public function getConn() {

            return $this->conn;
        }
    }
?>