<?php

    // start nebo pokračování
    session_start();

    // --------------------------------------------------------------------------------------- //

    // první inicializace
    if(!isset($_SESSION["page"])) {

        $_SESSION["page"] = "landing";
    }

    if(!isset($_SESSION["blog"])) {

        $_SESSION["blog"] = "-1";
    }

    if(!isset($_SESSION["filtr"])) {
        $_SESSION["filtr"] = "";
    }

    if(!isset($_SESSION["logged"])) {

        $_SESSION["logged"] = false;
    }

    if(!isset($_SESSION["loggingIn"])) {

        $_SESSION["loggingIn"] = false;
    }

    if(!isset($_SESSION["loggedId"])) {

        $_SESSION["loggedId"] = -1;
    }

    if(!isset($_SESSION["formSet"])) {
        $_SESSION["formSet"] = "";
    }

    if(!isset($_SESSION["formEdit"])) {
        $_SESSION["formEdit"] = false;
    }

    if(!isset($_SESSION["formError"])) {
        $_SESSION["formError"] = false;
    }

    // --------------------------------------------------------------------------------------- //

    // získání aktuální stránky pomocí GET
    if(isset($_GET["page"])) {

        $_SESSION["page"] = (String) $_GET["page"];
    }

    if(isset($_GET["blog"])) {

        $_SESSION["blog"] = (Integer) $_GET["blog"];
    }

    if(isset($_GET["search"])) {

        $_SESSION["filtr"] = (String) $_GET["search"];
    }

    // --------------------------------------------------------------------------------------- //

    // pokud se někdo přihlašuje/odhlašuje -> získání dat pomocí POST
    if(isset($_POST["log"])) {

        $_SESSION["loggingIn"] = true;
    }

    if(isset($_POST["logOUT"])) {

        $_SESSION["logged"] = false;
        $_SESSION["loggingIn"] = false;
        $_SESSION["formSet"] = "";
    }

    // --------------------------------------------------------------------------------------- //
    
    // editor -> získání dat pomocí POST
    if(isset($_POST["set"])) {
    
        $_SESSION["formSet"] = $_POST["set"];
    }

    if(isset($_POST["clear"])) {
    
        $_SESSION["formSet"] = "";
    }

    $_SESSION["formEdit"] = isset($_POST["save"]) || isset($_POST["new"]);
?>