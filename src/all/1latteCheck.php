<?php

    // šablonovací systém latte
    require_once("vendor/autoload.php");
    $latte = new Latte\Engine;
    $latte->setTempDirectory('temp');

    // přidání filtrů pro latte
    function limitWords($text, $limit) {
        $words = explode(' ', $text);
        if (count($words) > $limit) {
            return implode(' ', array_slice($words, 0, $limit)) . '...';
        }
        return $text;
    }    

    $latte->addFilter('limitWords', 'limitWords');
?>