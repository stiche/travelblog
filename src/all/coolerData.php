<?php

    // objekt pro práci s daty z databáze
    class dataSlave{

        public $users;
        public $places;
        public $blogs;

        public function setAllData($conn) {

            // 1)

            $sql1 = "SELECT * FROM users";

            $result1 = $conn->query($sql1);

            if($result1->num_rows > 0) {

                while($row = $result1->fetch_assoc()) {

                    $this->users[$row["id"]] = $row;
                }
            }

            // 2)

            $sql2 = "SELECT * FROM places";

            $result2 = $conn->query($sql2);

            if($result2->num_rows > 0) {

                while($row = $result2->fetch_assoc()) {

                    $this->places[$row["id"]] = $row;
                }
            }

            // 3)

            $sql3 = "SELECT * FROM blogs";

            $result3 = $conn->query($sql3);

            if($result3->num_rows > 0) {

                while($row = $result3->fetch_assoc()) {

                    $this->blogs[$row["id"]] = $row;
                }
            }

        }
    }
?>