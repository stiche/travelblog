<?php

    // objekt pro přihlašování 
    class mrLogger{

        private $users;

        private $id;

        public function __construct($u) {
            
            $this->users = $u;
        }
        
        public function isLogged() {

            if(!isset($_POST["login"]) || !isset($_POST["password"])) {

                return false;
            } else {

                return $this->OK($_POST["login"],$_POST["password"]);
            }
        }

        private function OK($login,$password) {

            foreach ($this->users as $key) {
                
                if($key["login"] === $login && $key["password"] === $password) {

                    $this->id = $key["id"];
                    
                    return true;
                }
            }

            return false;
        }

        public function getId() {

            return $this->id;
        }
    } 
?>