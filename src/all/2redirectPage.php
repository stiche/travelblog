<?php

    // třída pro přesměrování
    class redirecter{
        
        public function render($val, $latte, $params) {

            try {
                
                // při pokusu o neoprávněný přástup do editoru
                if($val === "editor") {

                    if(!($_SESSION["logged"] || $_SESSION["loggingIn"])) {
                        
                        if(!isset($_POST["editor"])){
                            
                            $val = "error";
                        }
                    }
                }

                $latte->render("templates/$val.latte", $params);
            } catch (\Throwable $th) {
                
                $latte->render("templates/error.latte", $params);
            }
        }
    }
?>