<?php

    // propejení všeho 
    $all = scandir("./src/all");

    for($i = 2;$i < count($all);$i++) {

        include "./src/all/".$all[$i];
    }

    // načtení data z databáze
    $m = new dataMaster;

    $m->setConn("localhost","root","","blog");

    // zformátování data na informace
    $s = new dataSlave;

    $s->setAllData($m->getConn());

    // případné přihlášení
    if($_SESSION["loggingIn"]) {
        
        $l = new mrLogger($s->users);

        $_SESSION["logged"] = $l->isLogged();
    }   

    // pokud přihlašovací údaje sedí
    if($_SESSION["logged"] && $_SESSION["loggingIn"]) {
    
        $_SESSION["page"] = "editor";

        $_SESSION["loggedId"] = $l->getId();

        header("Location: ?page=editor");
    }

    // případná editace dat pomocí editoru
    if($_SESSION["page"] == "editor" && $_SESSION["formEdit"]) {

        $f = new formerMaster($s->blogs, $s->users, $s->places);

        $f->update($m->getConn());
    }

    $x = new folderMan();

    // nahrání obrázku
    if(isset($_POST["imgUp"])) {
        $x->uploadImage();
    }

    // paramtery pro latte
    $params = [
        "users" => $s->users,
        "places" => $s->places,
        "blogs" => $s->blogs,
        "blogID" => $_SESSION["blog"],
        "filtr" => $_SESSION["filtr"],
        "logged" => $_SESSION["logged"],
        "loggingIn" => $_SESSION["loggingIn"],
        "loggedId" => $_SESSION["loggedId"],
        "formView" => $_SESSION["formSet"],
        "formError" => $_SESSION["formError"],
        "imgPaths" => $x->getPaths()
    ]; 

    // načtení stránky
    $r = new redirecter;

    $r->render($_SESSION["page"], $latte, $params);
?>