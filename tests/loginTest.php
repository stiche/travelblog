<?php
use Tester\Assert;
require '../src/all/loggingIn.php';


{
function testLoginuArgs()
    {
      return[
        ["user1", "heslo",1],//test pro existujiciho uzivatele

        ["", "heslo",1],//test pro vadny vysledek: prazdne login
        ["user1", "",1],//test pro vadny vysledek: prazdne heslo
        ["user1", "heslo",""],//test pro vadny vysledek: prazdne id

        ["user1", "heslo",99],//test pro vadny vysledek: neexistujici id
        ["user69", "heslo",1],//test pro vadny vysledek: neexistujici uzivatel
        ["user1", "user1", "1"],//test pro vadny vysledek: nespravne heslo

        ["user1", "heslo", "karelHacker69"],//test pro vadny vysledek: Stringove id
        [1, "heslo", 1],//test pro vadny vysledek: chybny typ loginu
        ["user1", 1, 1],//test pro vadny vysledek: chybny typ hesla

      ];
    }
   /**
 * @dataProvider testLoginuArgs
 */

    function testLoginu($log, $heslo, $id)
    {
      $instanceLoginu = new mrLogger;
      Assert::same($instanceLoginu->OK(), true);//test funkce OK($login, $password)
      Assert::same($instanceLoginu->getId(),$id);//test funkce getId()

    }
}